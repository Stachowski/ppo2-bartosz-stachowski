﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    /// <summary>
    /// Przechowuje większość kodu
    /// </summary>
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tworzy listę, do której dodawani będą studenci
        /// </summary>
        List<Student> listaStudentow = new List<Student>(); // twoorzymy listę studentów

        /// <summary>
        /// klasa student
        /// </summary>
        public class Student
        {
            public string imie, nazwisko, pesel, indeks;
            public Student() { }
            /// <summary>
            /// Konstruktor
            /// </summary>
            /// <param name="imie">Przechowuje imię studenta</param>
            /// <param name="nazwisko">Przechowuje nazwisko studenta</param>
            /// <param name="pesel">Przechowuje pesel studenta</param>
            /// <param name="indeks">Przechowuje numer indeksu studenta</param>
            public Student(string imie, string nazwisko, string pesel, string indeks)
            {
                this.imie = imie;
                this.nazwisko = nazwisko;
                this.pesel = pesel;
                this.indeks = indeks;
               
            }


        }

        /// <summary>
        /// Kontrola błedu.
        /// </summary>
        /// <returns>Zwraca true jeśli pola są puste, false jeśli są uzupełnione </returns>
        /// <remarks> 
        /// Sprawdza czy, któreś z pól formularza jest puste.
        /// <para>Działa to na zasadzie if który sprawdza ilość znaków w polu i jeśli wynosi ona zero to wywala błąd</para>
        /// <para><code> if ((textBoxImie.Text.Length == 0) || (textBoxNazwisko.Text.Length == 0) || (textBoxPesel.Text.Length == 0) ||(textBoxIndeks.Text.Length == 0)) return true;</code></para>
        /// </remarks> 
        public bool error() // sprawdza ile znaków jest w polach tekstowych, jeśli są puste czyli 0 znaków zwraca true
        {
            if ((textBoxImie.Text.Length == 0) ||
                (textBoxNazwisko.Text.Length == 0) ||
                (textBoxPesel.Text.Length == 0) ||
                (textBoxIndeks.Text.Length == 0)) return true;
            else return false;

        }

        /// <summary>
        /// Po użyciu dodaje naszego studenta
        /// </summary>
        /// <remarks> 
        /// Na samym początku jest kontrola błędu, która używa error(),
        /// <para>W przypadku gdy pola będą puste wykonuje się if</para>
        /// <para><code>if (error() == true) MessageBox.Show("ERROR", "ERROR ", MessageBoxButtons.OK);</code></para>
        /// <para>Jeśli wszystko jest ok to zmienne przybierają wartości pól</para>
        /// <para><code>string imie = textBoxImie.Text;</code></para>
        /// <para>A textBoxy zostają wyczyszczone</para>
        /// <para><code>textBoxImie.Text = null;</code></para>
        /// W ten sposób sudent dodany zostaje do listy
        /// <para><code>listaStudentow.Add(new Student(imie, nazwisko, pesel, indeks)); </code></para>
        /// <para>A następnie zostaje mu przypisane id</para>
        /// <para><code>Lista.Items.Add("id " + listaStudentow.Count());</code></para>
        /// </remarks> 
        private void buttonDodaj_Click(object sender, EventArgs e)
        {
            
            if (error() == true) MessageBox.Show("ERROR", "ERROR ", MessageBoxButtons.OK); // potencjalna kontrola błędu
            // jeśli pola będą puste pojawi się okienko z komunikatem 'ERROR'
            else
            {
                // zmienne przybierają wartości wpisane w textBox
                string imie = textBoxImie.Text;
                string nazwisko = textBoxNazwisko.Text;
                string pesel = textBoxPesel.Text;
                string indeks = textBoxIndeks.Text;

                //ustawia textBoxy co by były puste w środku po dodaniu              
                textBoxImie.Text = null;
                textBoxNazwisko.Text = null;
                textBoxPesel.Text = null;
                textBoxIndeks.Text = null;


                // dodajemy do listy cały obiekt klasy student
                listaStudentow.Add(new Student(imie, nazwisko, pesel, indeks)); 
                //wychodzi na to że po kliknięciu tworzymy obiekt który dodawany jest do naszej listy
                Lista.Items.Add("id " + listaStudentow.Count());

            }
        }

        /// <summary>
        /// Związane z naszym listBoxem
        /// </summary>
        /// <remarks> 
        /// Można powiedzieć że if wyczekuje na kliknięcie, w któryś z obiektów listy.
        /// <para><code>if ((Lista.SelectedIndex < Lista.Items.Count) && (Lista.SelectedIndex >= 0))</code></para>
        /// <para>Gdy to nastąpi "s" przybierze wartość, na podstawie, której będzie można wyświetlić informacje w textBoxach2 </para>
        /// <para><code>Student s = listaStudentow.ElementAt(Lista.SelectedIndex); </code></para>
        /// <para><code>textBoxImie2.Text = s.imie;</code></para>

        /// </remarks> 
        private void Lista_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((Lista.SelectedIndex < Lista.Items.Count) && (Lista.SelectedIndex >= 0))
            //SelectedIndex - Pobiera lub ustawia liczony od zera Indeks aktualnie wybranego elementu w ListBox.
            //items - Pobiera elementy ListBox.

            {
                Student s = listaStudentow.ElementAt(Lista.SelectedIndex); 
                textBoxImie2.Text = s.imie;
                textBoxNazwisko2.Text = s.nazwisko;
                textBoxPesel2.Text = s.pesel;
                textBoxIndeks2.Text = s.indeks;
                
            }
        }

        /// <summary>
        /// Kontrola błedu.
        /// </summary>
        /// <returns>Zwraca true jeśli pola są puste, false jeśli są uzupełnione </returns>
        /// <remarks> 
        /// Sprawdza czy, któreś z pól formularza jest puste.
        /// <para>Działa to na zasadzie if który sprawdza ilość znaków w polu i jeśli wynosi ona zero to wywala błąd</para>
        /// <para><code> if ((textBoxImie2.Text.Length == 0) || (textBoxNazwisko2.Text.Length == 0) || (textBoxPesel2.Text.Length == 0) ||(textBoxIndeks2.Text.Length == 0)) return true;</code></para>
        /// </remarks> 
        public bool error2()
        {
            if ((textBoxImie2.Text.Length == 0) || 
                (textBoxNazwisko2.Text.Length == 0) || 
                (textBoxPesel2.Text.Length == 0) || 
                (textBoxIndeks2.Text.Length == 0)) return true;
            else return false; // to samo zastosowanie
        }

        /// <summary>
        /// Pozwala edytować wartości obiektu klasy Student
        /// </summary>
        /// <remarks> 
        /// Na samym początku jest kontrola błędu, która używa error(),
        /// <para>W przypadku gdy pola będą puste wykonuje się if</para>
        /// <para><code>if (error2() == true) MessageBox.Show("ERROR", "ERROR ", MessageBoxButtons.OK);</code></para>
        /// <para>Jeśli pola nie są puste to działa to tak samo jak Lista_SelectedIndexChanged()</para>
        /// </remarks> 
        private void buttonEdytuj_Click(object sender, EventArgs e)
        {

            if (error2() == true) MessageBox.Show("ERROR", "ERROR ", MessageBoxButtons.OK); // ponownie okienko error
            else
            {
                //działa to tak że tworzymy nowy obiekt i nadpisujemy stary
                Student s = listaStudentow.ElementAt(Lista.SelectedIndex);
                s.imie = textBoxImie2.Text;
                s.nazwisko = textBoxNazwisko2.Text;
                s.pesel = textBoxPesel2.Text;
                s.indeks = textBoxIndeks2.Text;
            }
            
        }
    }
}
