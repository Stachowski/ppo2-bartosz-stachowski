package paint;

import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import java.util.Random;

/// <summary>
/// Znajduja sie tutaj metody takie jak nadawanie koloru czy tez zapis ustawien myszki
/// </summary>
public class Rysowanko extends JComponent {
	
	
	private static final long serialVersionUID = 1L;

	/// <summary>
	/// obszar, po ktorym mozna bedzie rysowac
	/// </summary>
	private Image obraz; 
	
	/// <summary>
	/// obiekt graficzny
	/// </summary>
	private Graphics2D grafika; 
	
	/// <summary>
	/// parametry dla myszki
	/// </summary>
	private int terazX, terazY, oldX, oldY;
	
	
	
	Random r = new Random();
	
	
	/// <summary>
	/// System rysowania
	/// </summary>
	/// <remarks> 
    /// Najpierw okresla czy komponent powinien uzywac bufora do malowania
    /// <para><code>setDoubleBuffered(false);	</code></para>
	/// <para>Nasluchuje i odbiera zdarzenia myszy</para>
	/// <para><code>addMouseListener(new MouseAdapter()</code></para>
	/// <para>Zapisuje ustawienie myszki kiedy jest klikana</para>
	/// <para><code>public void mousePressed(MouseEvent a) {</code></para>
	/// <para><code>oldX = a.getX();</code></para>
	/// <para><code>oldY = a.getY();}</code></para>
	/// <para>Odbiera zdarzenia ruchu myszy</para>
	/// <para><code>addMouseMotionListener(new MouseMotionAdapter()</code></para>
	/// <para> wspolrzedne x i y gdy mysz jest poruszana</para>
	/// <para><code>terazX = a.getX();</code></para>
	/// <para><code>terazY = a.getY();</code></para>
	/// </remarks>

	
	
	
	
	public Rysowanko()
	{
		setDoubleBuffered(false);
		addMouseListener(new MouseAdapter(){

			
			public void mousePressed(MouseEvent a) {
				//Zapisuje ustawienie myszki kiedy jest klikana
				oldX = a.getX();
				oldY = a.getY();
			}	
			
		});
		
		addMouseMotionListener(new MouseMotionAdapter() {

			public void mouseDragged(MouseEvent a) {
				// wsp�lrzedny x,y kiedy ruszam mysza
				terazX = a.getX();
				terazY = a.getY();
				
				if (grafika != null)
				{
					//rysuje linie je�li "grafika" nei jest rowna 0
					grafika.drawLine(oldX, oldY, terazX, terazY);
					//no  wiadomo xD
					repaint();
					//wiadomo przypisuje obezne kordy ttym starym
					
					oldX = terazX;
					oldY = terazY;
				}
			}
				
		});
	}
	
	/// <summary>
	/// Panel, po ktorym mozna rysowac
	/// </summary>
	/// <remarks> 
    /// Najpierw tworzony jest obszar po, ktorym mozna rysowac
    /// <para><code>obraz = createImage(getSize().width, getSize().height);	</code></para>
	/// <para><code>grafika = (Graphics2D) obraz.getGraphics();	</code></para>
	/// <para>Potem wygladza sie krawedzie</para>
	/// <para><code>grafika.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);	</code></para>
    /// </remarks> 
	protected void paintComponent(Graphics g)
	{
		if (obraz == null)
		{
			//tworzenie obrazka
			obraz = createImage(getSize().width, getSize().height);	
			grafika = (Graphics2D) obraz.getGraphics();
			//wyg�adzanie kraw�dzi
			grafika.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			clear();
		}
		g.drawImage(obraz, 0, 0, null);
	}

	/// <summary>
	/// Czysci nasz obraz
	/// </summary>
	/// <remarks>
	/// Tak naprawde to zalewa wszystko na bialo
	/// <para><code>grafika.setPaint(Color.white);</code></para>
	/// <para><code>grafika.fillRect(0, 0, getSize().width, getSize().height);</code></para>
	/// <para><code>grafika.setPaint(Color.black);</code></para>
	/// <para><code>repaint();</code></para>
	/// </remarks>

	public void clear()
	{
		grafika.setPaint(Color.white);
		grafika.fillRect(0, 0, getSize().width, getSize().height);
		grafika.setPaint(Color.black);
		repaint();
	}
	
	/// <summary>
	/// Zmienia kolor tla
	/// </summary>
	/// <remarks>
	/// Domyslnie ustawione na czarny
	/// <para><code>grafika.fillRect(0, 0, getSize().width, getSize().height);</code></para>
	/// <para><code>grafika.setPaint(Color.black);</code></para>
	/// <para><code>repaint();</code></para>
	/// </remarks>
	public void tlo()
	{
		grafika.fillRect(0, 0, getSize().width, getSize().height);
		grafika.setPaint(Color.black);
		repaint();
	}
	
	
	/// <summary>
	/// Pozwala korzystac z czerwonego koloru
	/// </summary>
	/// <remarks>
	/// <para><code>grafika.setPaint(Color.red);</code></para>
	/// </remarks>
	public void red() {
		
		grafika.setPaint(Color.red);
	}
	
	/// <summary>
	/// Pozwala korzystac z czarnego koloru
	/// </summary>
	/// <remarks>
	/// Analogicznie do red()
	/// </remarks>
	public void black() {
		grafika.setPaint(Color.black);
	}
	
	/// <summary>
	/// Pozwala korzystac z koloru magenta
	/// </summary>
	/// <remarks>
	/// Analogicznie do red()
	/// </remarks>
	public void magenta() {
		grafika.setPaint(Color.magenta);
	}
	/// <summary>
	/// Pozwala korzystac z zielonego koloru
	/// </summary>
	/// <remarks>
	/// Analogicznie do red()
	/// </remarks>
	public void green() {
		grafika.setPaint(Color.green);
	}
	/// <summary>
	/// Pozwala korzystac z niebieskiego koloru
	/// </summary>
	/// <remarks>
	/// Analogicznie do red()
	/// </remarks>
	public void blue() {
		grafika.setPaint(Color.blue);
	}
	
	/// <summary>
	/// Umieszcza kwadrat w losowe miejsce
	/// </summary>
	/// <remarks>
	/// <para>Najpierw przypisuje sie losowo wartosc dla a b i c</para>
	/// <para><code>int a = r.nextInt(100);</code></para>
	/// <para>nastepnie rogi kwadratu zostaja umieszczone na ekranie i polaczone</para>
	/// <para><code>grafika.drawLine(a+b, a+c, a+b, 2*a+c);</code></para>
	/// </remarks>
	public void kwadrat() {
		int a = r.nextInt(100);
		int b = r.nextInt(600);	
		int c = r.nextInt(300);	
		grafika.drawLine(a+b, a+c, a+b, 2*a+c); // (a200, b50, a200, c450)
		grafika.drawLine(a+b, 2*a+c, 2*a+b, 2*a+c); // (a200, c450, d600, c450)
		grafika.drawLine(2*a+b, 2*a+c, 2*a+b, a+c); // (d600, c450, d600, b50)
		grafika.drawLine(a+b, a+c, 2*a+b, a+c); // (a200, b50, d600, b50)
		repaint(); // czyli jakby odswieza ekran
	}
	/// <summary>
	/// Umieszcza kolo w losowe miejsce
	/// </summary>
	/// <remarks>
	/// <para>Najpierw przypisuje sie losowo wartosc dla a b i c</para>
	/// <para><code>int a = r.nextInt(500);</code></para>
	/// <para>nastepnie kolo zostaja umieszczone na ekranie</para>
	/// <para><code>grafika.drawOval(b, c, a, a);</code></para>
	/// </remarks>
	public void koleczko() {
		int a = r.nextInt(500);
		int b = (r.nextInt(400));
		int c = (r.nextInt(200));
		grafika.drawOval(b, c, a, a); // 200 , 50 , 400 , 400
		repaint(); 
	}
	
	/// <summary>
	/// Umieszcza trojkat w losowe miejsce
	/// </summary>
	public void trojkat() {
		int a = r.nextInt(700);
		int b = r.nextInt(500);
		int c = r.nextInt(700);
		int d = r.nextInt(500);
		int e = r.nextInt(700);
		grafika.drawLine(a, b, c, b); //(a226, b350, c574, b350);
		grafika.drawLine(a, b, d, e); //(a226, b350, d400, e50);
		grafika.drawLine(c, b, d, e); //(c574, b350, d400, e50);
		repaint(); 		
	}
	/// <summary>
	/// Umieszcza prostokat w losowe miejsce
	/// </summary>
	public void prostokat() {
		int a = r.nextInt(700);
		int b = r.nextInt(500);
		int c = r.nextInt(500);
		int d = r.nextInt(700);
		grafika.drawLine(a, b, a, c); // (a200, b50, a200, c450)
		grafika.drawLine(a, c, d, c); // (a200, c450, d600, c450)
		grafika.drawLine(d, c, d, b); // (d600, c450, d600, b50)
		grafika.drawLine(a, b, d, b); // (a200, b50, d600, b50)
		repaint(); // czyli jakby odswieza ekran
	}

	
		
	
}
	
	
		
	
	
	



