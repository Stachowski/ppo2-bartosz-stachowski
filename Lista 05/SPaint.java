package paint;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/// <summary>
/// Wyglad okna, guziki itp
/// </summary>
public class SPaint {
	
	
	JButton czyscicielK, czarnyK, niebieskiK, zielonyK, czerwonyK, magentaK,kwadratK,
	koloK,trojkatK,prostokatK,tloK;
	Rysowanko rysowanko;
	ActionListener actionListener = new ActionListener(){
		
		/// <summary>
		/// wywolana po wystapieniu akcji
		/// </summary>
		public void actionPerformed(ActionEvent a){
			
			if(a.getSource() == czyscicielK){
				rysowanko.clear();
			}else if (a.getSource()== czarnyK) {
				rysowanko.black();
			}else if (a.getSource()== niebieskiK) {
				rysowanko.blue();
			}else if (a.getSource()== zielonyK) {
				rysowanko.green();
			}else if (a.getSource()== czerwonyK) {
				rysowanko.red();
			}else if (a.getSource()== magentaK) {
				rysowanko.magenta();	
			}else if (a.getSource()== kwadratK) {
				rysowanko.kwadrat();
			}else if (a.getSource()== koloK) {
				rysowanko.koleczko();
			}else if (a.getSource()== trojkatK) {
				rysowanko.trojkat();
			}else if (a.getSource()== prostokatK) {
				rysowanko.prostokat();
			}else if (a.getSource()== tloK) {
				rysowanko.tlo();
			}
			
		}
	};


	public static void main(String[] args)
	{
		new SPaint().show();
	}
	
	
	
	/// <summary>
	/// wyswietla nasz paint
	/// </summary>
	/// <remarks> 
    /// Tworzy okno wraz z jego nazwa
    /// <para><code>JFrame ramka = new JFrame("To niezly Paint'cik :D");</code></para>
	/// <para><code>Container content = ramka.getContentPane();	</code></para>
	/// <para>ustawia layouta do glownego panelu</para>
	/// <para><code>content.setLayout(new BorderLayout());</code></para>
	/// <para>tworzy obszar do rysowania</para>
	/// <para><code>rysowanko = new Rysowanko();	</code></para>
	/// <para>dodaje do glownego panelu ("cointent pane")</para>
	/// <para><code>content.add(rysowanko, BorderLayout.CENTER);</code></para>
	
	/// <para>//tworzy kontrolki dla kololrow i czyszczenia	</para>
	/// <para><code>JPanel kontrolki = new JPanel();</code></para>
	/// <para>Przyklad:	</para>
	/// <para><code>czyscicielK = new JButton("PanCzy�ciciel");	</code></para>
	/// <para><code>czyscicielK.addActionListener(actionListener);	</code></para>
	/// <para>Podobnie jest z figurami	</para>
	/// <para><code>JPanel figury = new JPanel();	</code></para>
	/// <para><code>koloK = new JButton("Ko�eczko");	</code></para>
	/// <para><code>koloK.addActionListener(actionListener);	</code></para>
	
	/// <para>Dodawanie buttonow do panelu</para>
	/// <para><code>kontrolki.add(czarnyK);	</code></para>
	/// <para><code>figury.add(kwadratK);</code></para>
	
	/// <para>dodaje do kontent panela	</para>
	/// <para><code>content.add(kontrolki, BorderLayout.NORTH);	</code></para>
	/// <para><code>content.add(figury, BorderLayout.SOUTH);	</code></para>

	/// <para>	rozmiar okna</para>
	/// <para><code>ramka.setSize(800, 600);</code></para>
	/// <para>pozwala zamknac okno</para>
	/// <para><code>ramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	</code></para>
	
	/// <para>pokazduje Spainta	</para>
	/// <para><code>ramka.setVisible(true);	</code></para>
	/// </remarks>
	
	public void show()
	{
		//tworzy g��wn� ramke	
				JFrame ramka = new JFrame("To niez�y Paint'cik :D");
				Container content = ramka.getContentPane();	
				//ustawia layouta do g�ownego panelu
				content.setLayout(new BorderLayout());
				//tworzy obszar do rysowania
				rysowanko = new Rysowanko();
				//dodaje do g�ownego panelu ("cointent pane")
				content.add(rysowanko, BorderLayout.CENTER);
				
				//tworzy kontrolki dla kololrow i czyszczenia
				JPanel kontrolki = new JPanel();
				
				czyscicielK = new JButton("PanCzy�ciciel");
				czyscicielK.addActionListener(actionListener);
				czarnyK = new JButton("Czarniutki");
				czarnyK.addActionListener(actionListener);
				niebieskiK = new JButton("Niebie�ciutki");
				niebieskiK.addActionListener(actionListener);
				zielonyK = new JButton("Zieloniutki");
				zielonyK.addActionListener(actionListener);
				czerwonyK = new JButton("Czerwoniutki");
				czerwonyK.addActionListener(actionListener);
				magentaK = new JButton("Magentowy");
				magentaK.addActionListener(actionListener);

				
				
				//panel dla figur i t�a
				JPanel figury = new JPanel();
				
				koloK = new JButton("K�eczko");
				koloK.addActionListener(actionListener);
				kwadratK = new JButton("Kwadracik");
				kwadratK.addActionListener(actionListener);
				trojkatK = new JButton("Tr�jk�cik");
				trojkatK.addActionListener(actionListener);
				prostokatK = new JButton("Prostok�cik");
				prostokatK.addActionListener(actionListener);
				tloK = new JButton("T�o");
				tloK.addActionListener(actionListener);
	
				
				
				//dodwanie do panelu
				kontrolki.add(czarnyK);
				kontrolki.add(zielonyK);
				kontrolki.add(niebieskiK);
				kontrolki.add(czerwonyK);
				kontrolki.add(magentaK);
				kontrolki.add(czyscicielK);	
				
				
				figury.add(kwadratK);
				figury.add(prostokatK);
				figury.add(koloK);
				figury.add(trojkatK);
				figury.add(tloK);
				
				
		
				
				//dodaje do kontent panela
				content.add(kontrolki, BorderLayout.NORTH);
				content.add(figury, BorderLayout.SOUTH);
				
				ramka.setSize(800, 600);
				
				//pozwala zamknac okno
				ramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				//pokazduje Spainta
				ramka.setVisible(true);
	}
	
	
};
	

	

	
	

